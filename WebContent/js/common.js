function $ ( objId )
{
	return document.getElementById ( objId );
}

// function to set a cookie
function SetCookie ( name, value )
{
	expires = new Date();
	expires.setTime(expires.getTime() + (1000 * 86400 * 365));
	document.cookie = name + "=" + escape(value) + "; expires=" + expires.toGMTString() +  "; path=/";
}
/* 
* 获得时间差,时间格式为 年-月-日 小时:分钟:秒 或者 年/月/日 小时：分钟：秒 
* 其中，年月日为全格式，例如 ： 2010-10-12 01:00:00 
* 返回精度为：秒，分，小时，天 
*/ 
function GetDateDiff(startTime, endTime, diffType) { 
//将xxxx-xx-xx的时间格式，转换为 xxxx/xx/xx的格式 
startTime = startTime.replace(/\-/g, "/"); 
endTime = endTime.replace(/\-/g, "/"); 
//将计算间隔类性字符转换为小写 
diffType = diffType.toLowerCase(); 
var sTime = new Date(startTime); //开始时间 
var eTime = new Date(endTime); //结束时间 
//作为除数的数字 
var divNum = 1; 
switch (diffType) { 
	case "second": 
		divNum = 1000; 
		break; 
	case "minute": 
		divNum = 1000 * 60; 
		break; 
	case "hour": 
		divNum = 1000 * 3600; 
		break; 
	case "day": 
		divNum = 1000 * 3600 * 24; 
		break; 
	default: 
		break; 
	} 
	return parseInt((eTime.getTime() - sTime.getTime()) / parseInt(divNum)); 
} 

// #############################################################################
// function to retrieve a cookie
function GetCookie ( name )
{
	cookie_name = name + "=";
	cookie_length = document.cookie.length;
	cookie_begin = 0;
	while (cookie_begin < cookie_length)
	{
		value_begin = cookie_begin + cookie_name.length;
		if (document.cookie.substring(cookie_begin, value_begin) == cookie_name)
		{
			var value_end = document.cookie.indexOf (";", value_begin);
			if (value_end == -1)
			{
				value_end = cookie_length;
			}
			return unescape(document.cookie.substring(value_begin, value_end));
		}
		cookie_begin = document.cookie.indexOf(" ", cookie_begin) + 1;
		if (cookie_begin == 0)
		{
			break;
		}
	}
	return null;
}

// #############################################################################
// function to delete a cookie
function DelCookie ( name )
{
	var expireNow = new Date();
	document.cookie = name + "=" + "; expires=Thu, 01-Jan-70 00:00:01 GMT" +  "; path=/";
}

// 獲取 URL 变量
function GetURLVar ( Var, URL )
{
	var URL = URL == null ? window.location.href : URL;
	var re = new RegExp ( ".*[\?|&]" + Var + "=([^&]+).*", "g" );
	if ( URL.match ( re ) )
	{
		Val = URL.replace ( re, "$1" );
		return Val;
	}
	return;
}

function PopUp ( URL, Width, Height )
{
	var obj = document.getElementById ( "frmPopUp" );
	if ( obj )
	{
		obj.src = "/PopUp.html?url=" + escape ( URL ) + "&width=" + Width + "&height=" + Height;
	}
	else
	{
		OpenWindow ( URL, Width, Height );
	}
}

function OpenWindow ( URL, Width, Height )
{
	if ( !Width || Width == "undefined" )
	{
		Width = 400;
	}
	if ( !Height || Height == "undefined" )
	{
		Height = 300;
	}
	if ( document.all )
	{
		showModelessDialog ( URL, '', "dialogWidth:" + Width + "px; dialogHeight:" + Height + "px; status:no; resizable:yes" );
	}
	else
	{
		window.open ( URL, '', "width:" + Width + "px; height:" + Height + "px; status:no; resizable:yes" );
	}
}

function setSelectOptions(the_form, the_select, do_check)
{
    var selectObject = document.forms[the_form].elements[the_select];
    var selectCount  = selectObject.length;

    for (var i = 0; i < selectCount; i++) {
        selectObject.options[i].selected = do_check;
    }
    return true;
}

function SetChecked ( val, obj, name )
{
	len = obj.elements.length;
	var i=0;
	for( i=0 ; i<len ; i ++ )
	{
		if ( obj.elements[i].name == name )
		{
			obj.elements[i].checked = val;
		}
	}
}

// 错误提示
function showMessage ( msg, obj )
{
	if ( obj )
	{
		obj.innerHTML = msg;
	}
	else
	{
		alert ( msg );
	}
}

// 字數限制函數
function LengthLimit ( obj, Limit, objShow, objAlert )
{
	var Len = obj.value.length;
	if ( Len > Limit )
	{
		obj.value = obj.value.substring ( 0, Limit );
		Len = Limit;
		showMessage ( String.sprintf ( "字數超出限制, 最多 %d 字!", Limit ), objAlert );
	}
	if ( objShow = document.getElementById ( objShow ) )
	{
		objShow.innerHTML = Len;
	}	
}

// 列表搜索
function ClientSearch ( value, obj )
{
	if ( value != '' )
	{
		for ( var i = obj.selectedIndex + 1; i < obj.options.length; i ++ )
		{
			if ( obj.options[i].text.indexOf ( value, 0 ) >= 0 )
			{
				obj.selectedIndex = i;
				return true;
			}		
		}
	}
	obj.selectedIndex = 0;
}

// 列表項目移動
function moveOptions ( objFrom, objTo, errMsg, moveList )
{
	if ( typeof ( objFrom ) == 'string' )
	{
		objFrom = document.getElementById ( objFrom );
	}
	if ( typeof ( objTo ) == 'string' )
	{
		objTo = document.getElementById ( objTo );
	}

	moveList = moveList != null ? moveList : '';
	moveList = ',' + moveList + ',';
	if ( objFrom.selectedIndex == -1 && errMsg != null )
	{
		alert ( errMsg );
	}
	for ( var i = 0; i < objFrom.options.length; i ++ )
	{
		if ( moveList.match ( ',-1,' ) || moveList.match ( ',' + objFrom.options[i].value + ',' ) || objFrom.options[i].selected )
		{
			objTo.options.add ( new Option ( objFrom.options[i].text, objFrom.options[i].value ) );
			objFrom.options[i--] = null;
		}
	}
}

// 設置状态栏
function setStatus ( w, id )
{
	window.status = w;
	return true;
}
function clearStatus ()
{
	window.status = '';
}

// 显示物件
function showItem ( obj, posX, posY )
{
	if ( typeof (obj) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	try
	{
		obj.style.display = '';
		if ( posX != null ) obj.style.left = posX + 'px';
		if ( posY != null ) obj.style.top = posY + 'px';
		obj.focus ();
	}
	catch (e)
	{
	}
}

// 隐藏物件
function hideItem ( obj )
{
	if ( typeof (obj) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	try
	{
		obj.style.display = 'none';
	}
	catch (e)
	{
	}	
}

// 交替显示/隐藏物件
function itemShowHide ( obj )
{
	if ( typeof ( obj ) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	try
	{
		if ( obj.style.display != 'block' )
		{
			showItem ( obj );
		}
		else
		{
			hideItem ( obj );			
		}
	}
	catch (e)
	{
	}
}

// 獲取错误信息
function getError ( string )
{
	if ( string.substring ( 0, 6 ) == 'ERROR|' )
	{
		return string.substring ( 6, string.length );
	}
	else
	{
		return false;
	}
}

// Ajax 通用回调函數
function ajaxCallback ( Ret, obj )
{
	var ErrorMsg = getError ( Ret );
	if ( ErrorMsg )
	{
		showMessage ( ErrorMsg );
		hideItem ( obj );
	}
	else if ( Ret != '' )
	{
		showMessage ( Ret, obj );
		showItem ( obj );
	}
}

// 固定 Div
function stayDiv ( obj, top, left )
{
	if ( typeof ( obj ) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	top = top != null ? top : 0;
	left = left != null ? left : 0;
	obj.style.top = top + document.documentElement.scrollTop + 'px';
	obj.style.left = left + document.documentElement.scrollLeft + 'px';
	setTimeout ( "stayDiv('" + obj.id + "'," + top + "," + left + ")", 100 );
}


// Div 居中
function centerDiv ( obj )
{
	if ( typeof ( obj ) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	obj.style.top = '50%';
	obj.style.left = '50%';
	try
	{
		obj.style.marginLeft = ( 0 - obj.scrollWidth / 2 + document.documentElement.scrollLeft ) + 'px';
		obj.style.marginTop = ( 0 - obj.scrollHeight / 2 + document.documentElement.scrollTop ) + 'px';
	}
	catch (e)
	{
	}
	setTimeout ( "centerDiv('" + obj.id + "')", 1000 );
}

// 設置背景色
function setBg ( obj, color )
{
	if ( typeof ( obj ) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	try
	{
		obj.style.backgroundColor = color;
	}
	catch (e)
	{
	}
}

// 格式化輸出
function sprintf ()
{
	if ( sprintf.arguments.length < 2 )
	{
		return;
	}
	var data = sprintf.arguments[ 0 ];
	for( var k=1; k<sprintf.arguments.length; ++k )
	{
		switch( typeof( sprintf.arguments[ k ] ) )
		{
			case 'string':
			data = data.replace( /%s/, sprintf.arguments[ k ] );
			break;
			case 'number':
			data = data.replace( /%d/, sprintf.arguments[ k ] );
			break;
			case 'boolean':
			data = data.replace( /%b/, sprintf.arguments[ k ] ? 'true' : 'false' );
			break;
			default:
			break;
		}
	}
	return( data );
}

 if ( !String.sprintf ) String.sprintf = sprintf;

// 图层定位
function moveDivHere ( obj )
{
	try
	{
		if ( obj != null )
		{
			obj.innerHTML = '正在载入, 請稍候...';

			// 獲取當前鼠标坐标
			var posX = clsMouseCoords.x + 5;
			var posY = clsMouseCoords.y + 5;
			obj.style.left = posX + 'px';
			obj.style.top = posY + 'px';
		}
	}
	catch (e)
	{
	}
}

//复制URL地址
function setCopy(_sTxt)
{
	if( navigator.userAgent.toLowerCase().indexOf('ie') > -1)
	{
		clipboardData.setData('Text',_sTxt);
		alert ("網址“"+_sTxt+"”\n已經复制到您的剪贴板中\n您可以使用Ctrl+V快捷键粘贴到需要的地方");
	}
	else
	{
		prompt("請复制網站地址:",_sTxt); 
	}
}

//加入收藏
function addBookmark(site, url)
{
	if( navigator.userAgent.toLowerCase().indexOf('ie') > -1 )
	{
		window.external.addFavorite(url,site);
	}
	else if ( navigator.userAgent.toLowerCase().indexOf('opera') > -1 )
	{
		alert ("請使用 Ctrl+T 將本頁加入收藏夹");
	}
	else
	{
		alert ("請使用 Ctrl+D 將本頁加入收藏夹");
	}
}

// 语言包支持
function x_Lang ()
{
	var strInput = Lang[x_Lang.arguments[0]];
	var strParams = '';

	for( var k=1; k < x_Lang.arguments.length; ++k )
	{
		switch( typeof( x_Lang.arguments[ k ] ) )
		{
			case 'string':
				strParams += ", '" + x_Lang.arguments[ k ] + "'";
			break;
			case 'number':
				strParams += ", " + x_Lang.arguments[ k ] + "";
			break;
		}
	}
	if ( strParams != '' )
	{
		strEval = "strOutput = String.sprintf ( strInput" + strParams + " );";
		eval ( strEval );
	}
	else
	{
		strOutput = strInput;
	}	
	return ( strOutput );
}

// 獲取鼠标坐标
function mouseCoords ()
{
	this.x = 0;
	this.y = 0;

	this.getMouseCoords = function ( ev )
	{
		try
		{
			ev = ev || window.event;
			if ( ev.pageX || ev.pageY )
			{
				this.x = ev.pageX;
				this.y = ev.pageY;
			}
			else
			{
				this.x = ev.clientX + document.documentElement.scrollLeft - document.documentElement.clientLeft;
				this.y = ev.clientY + document.documentElement.scrollTop  - document.documentElement.clientTop;
			}
		}
		catch (e)
		{
		}
	}
}

var clsMouseCoords = new mouseCoords ();
document.onmousemove = function ( ev )
{
	clsMouseCoords.getMouseCoords ( ev );
}

// 獲得焦點
function setFocus ( objName )
{
	var obj;

	var objs = document.getElementsByName ( objName );
	if ( objs.length > 0 )
	{
		obj = objs.item(0);
	}
	else
	{
		obj = document.getElementById ( objName );
	}
	if ( obj )
	{
		obj.focus ();
	}
}

// 列表搜索
function clientSearch ( valueObj, obj )
{
	if ( typeof ( valueObj ) == 'string' )
	{
		valueObj = document.getElementById ( valueObj );
	}
	var value = valueObj.value;
	if ( value != '' )
	{
		if ( typeof ( obj ) == 'string' )
		{
			obj = document.getElementById ( obj );
		}
		for ( var i = obj.selectedIndex + 1; i < obj.options.length; i ++ )
		{
			if ( obj.options[i].text.indexOf ( value, 0 ) >= 0 )
			{
				obj.selectedIndex = i;
				return true;
			}		
		}
	}
	obj.selectedIndex = 0;
}

// 獲取選單值
function getOptionValues ( obj )
{
	if ( typeof ( obj ) == 'string' )
	{
		obj = document.getElementById ( obj );
	}

	var arrTypes = new Array ();
	for ( var i = 0; i < obj.options.length; i ++ )
	{
		arrTypes.push ( obj.options[i].value );
	}
	var listValue = arrTypes.join ( ',' );
	return listValue;
}
