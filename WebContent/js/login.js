function checkerLogin ( form, urlPassport )
{
	this.objSelf = "clsChkLogin";
	this.objForm = document.getElementById ( form );
	this.objMessage = document.getElementById ( "login_message" );
	this.urlPassport = urlPassport;

	this.CheckForm = function ()
	{
		if ( this.objForm.username.value == '' )
		{
			showMessage ( '請填寫您的通行證!', this.objMessage );
			this.objForm.username.focus ();
			return false;
		}
		if ( this.objForm.password.value == '' )
		{
			showMessage ( '請填寫您的登入密碼!', this.objMessage );
			this.objForm.password.focus ();
			return false;
		}
		/*		
		if ( this.objForm.code.value == '' )
		{
			showMessage ( '請填寫驗證碼, 驗證碼即輸入框右邊的彩色數字!', this.objMessage );
			this.objForm.code.focus ();
			return false;
		}
		*/
		this.objForm.submit ();
		return true;
	}
}
