package tw.com.twgame.constants;

import java.util.HashMap;

public class Config {
	public static final int NORMALLY = 1;
	public static final int SUSPENDED = 2;
	
	public static final String RETURN_SUCCESS = "0000";
	public static final String RETURN_INPUT_EMPTY = "0001";
	public static final String RETURN_DATA_NOT_FOUND = "0002";
	public static final String RETURN_TOKEN_ERROR = "003";
	public static final String RETURN_INPUT_ERROR = "004";
	public static final String RETURN_TIME_OUT = "0005";
	public static final String RETURN_FAIL = "0006";
	public static final String RETURN_SYSTEM_ERROR = "9999";
	
	public static final HashMap<String, String> RETURN_RESULT=new HashMap<String, String>();
	static{
		RETURN_RESULT.put(RETURN_SUCCESS, "success");
		RETURN_RESULT.put(RETURN_INPUT_EMPTY, "input empty");
		RETURN_RESULT.put(RETURN_DATA_NOT_FOUND, "data not found");
		RETURN_RESULT.put(RETURN_TOKEN_ERROR, "token error");
		RETURN_RESULT.put(RETURN_INPUT_ERROR, "input error");
		RETURN_RESULT.put(RETURN_TIME_OUT, "time out");
		RETURN_RESULT.put(RETURN_FAIL, "fail");
		RETURN_RESULT.put(RETURN_SYSTEM_ERROR, "system error");
	}
}
