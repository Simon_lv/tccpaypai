package tw.com.twgame.controller.api;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.com.twgame.common.encrypt.MD5Util;
import tw.com.twgame.constants.Config;
import tw.com.twgame.controller.BaseController;
import tw.com.twgame.model.Customer;
import tw.com.twgame.model.Invoice;
import tw.com.twgame.model.Member;
import tw.com.twgame.model.Payment;
import tw.com.twgame.model.PaymentForView;
import tw.com.twgame.service.CustomerManager;
import tw.com.twgame.service.InvoiceManager;
import tw.com.twgame.service.MemberManager;
import tw.com.twgame.service.PaymentManager;

@Controller
@Scope("request")
public class OrderApi extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(OrderApi.class);
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final SimpleDateFormat sdf0 = new SimpleDateFormat("yyyyMMddHHmmssS");
	
	@Autowired
	protected PaymentManager payManager;
	
	@Autowired
	protected MemberManager memberManager;
	
	@Autowired
	protected InvoiceManager invoiceManager;
	
	@Autowired
	private CustomerManager customerManager;
	
	@Autowired
	@Qualifier("appKey")
	private String appKey;
	
	@RequestMapping(value = "/order")
	public @ResponseBody Map<String, String> order(
			@RequestParam("payFrom") String payFrom,
			@RequestParam("orderId") String payOrderId, //orderId金流方 
			@RequestParam(value = "uid", required = false, defaultValue = "") String uid,
			@RequestParam("name") String name,
			@RequestParam("mobile") String mobile,
			@RequestParam("address") String address,
			@RequestParam("money") int money,
			@RequestParam("orderTime") String orderTime,
			@RequestParam("invType") int invType,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "invName", required = false) String invName,
			@RequestParam(value = "invPhone", required = false) String invPhone,
			@RequestParam(value = "invAddress", required = false) String invAddress,
			@RequestParam(value = "invTitle", required = false) String invTitle,
			@RequestParam(value = "invTaxId", required = false) String invTaxId,
			@RequestParam("token") String token ) {
		Map<String, String> result = new HashMap<String, String>();
		String code = Config.RETURN_SUCCESS;
		
		StringBuffer sf = new StringBuffer();
		sf.append("============== OrderApi order ====================\n");
		sf.append("input param [payFrom=").append(payFrom).append(", orderId=").append(payOrderId);
		sf.append(", uid=").append(uid).append(", name=").append(name).
		append(", mobile=").append(mobile).append(", address=").append(address).
		append(", money=").append(money).append(", orderTime=").append(orderTime).
		append(", invType=").append(invType).append(", email=").append(email).append(", invName=").append(invName).
		append(", invPhone=").append(invPhone).append(", invAddress=").append(invAddress).
		append(", invTitle=").append(invTitle).append(", invTaxId=").append(invTaxId).
		append(", token=").append(token).append("]"); 
		
		LOG.info(sf.toString());
		try {
			if (StringUtils.isBlank(payFrom) || StringUtils.isBlank(payOrderId) ||
				StringUtils.isBlank(name) || StringUtils.isBlank(mobile) ||
				StringUtils.isBlank(address) || money<=0 ||
				StringUtils.isBlank(orderTime)) {
				code = Config.RETURN_INPUT_EMPTY;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			Customer customer = customerManager.findByPayFrom(payFrom);
			if (!token.equalsIgnoreCase(MD5Util.crypt(payFrom+ payOrderId +mobile+money+ orderTime + invType +customer.getKey()))) {
				code = Config.RETURN_TOKEN_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			
			if(invType == 2 && StringUtils.isBlank(email))
			{
				code = Config.RETURN_INPUT_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			
			if(invType == 3 &&
					(
							StringUtils.isBlank(invName) ||
							StringUtils.isBlank(invPhone) ||
							StringUtils.isBlank(invAddress)
					)
			) {
				code = Config.RETURN_INPUT_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			Member member = saveMember(uid, payFrom, name, mobile, address, new Timestamp(sdf.parse(orderTime).getTime()), false);
			
			if(StringUtils.isBlank(uid)) {
				uid = member.getUid();
			}
			String currency = "TWD";
			Payment pay = savePayment(money, address, payFrom, new Timestamp(sdf.parse(orderTime).getTime()), 0, name, mobile, uid, 1, payOrderId, currency);
			if(StringUtils.isBlank(uid)) {
				member.setUid(member.getId()+"");
				memberManager.update(member);
			}
			saveInvoice(pay.getOrderId(), uid, payFrom, invType, email, invName, invPhone, invAddress, invTitle, invTaxId, new Timestamp(sdf.parse(orderTime).getTime()));
			
			result.put("orderId", pay.getOrderId());
			
			result.put("token", MD5Util.crypt(pay.getOrderId()+token+customer.getKey()));
		} catch(Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		result.put("code", code);
		result.put("message", Config.RETURN_RESULT.get(code).toString());
		LOG.info("result="+result);
		return result;
	}
	
	//需多傳入currency
	@RequestMapping(value = "/v2/order")
	public @ResponseBody Map<String, String> order2(
			@RequestParam("payFrom") String payFrom,
			@RequestParam("orderId") String payOrderId, //orderId金流方 
			@RequestParam(value = "uid", required = false, defaultValue = "") String uid,
			@RequestParam("name") String name,
			@RequestParam("mobile") String mobile,
			@RequestParam("address") String address,
			@RequestParam("money") int money,
			@RequestParam("currency") String currency,
			@RequestParam("orderTime") String orderTime,
			@RequestParam("invType") int invType,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "invName", required = false) String invName,
			@RequestParam(value = "invPhone", required = false) String invPhone,
			@RequestParam(value = "invAddress", required = false) String invAddress,
			@RequestParam(value = "invTitle", required = false) String invTitle,
			@RequestParam(value = "invTaxId", required = false) String invTaxId,
			@RequestParam("token") String token ) {
		Map<String, String> result = new HashMap<String, String>();
		String code = Config.RETURN_SUCCESS;
		
		StringBuffer sf = new StringBuffer();
		sf.append("============== OrderApi order ====================\n");
		sf.append("input param [payFrom=").append(payFrom).append(", orderId=").append(payOrderId);
		sf.append(", uid=").append(uid).append(", name=").append(name).
		append(", mobile=").append(mobile).append(", address=").append(address).
		append(", money=").append(money).append(", currency=").append(currency).append(", orderTime=").append(orderTime).
		append(", invType=").append(invType).append(", email=").append(email).append(", invName=").append(invName).
		append(", invPhone=").append(invPhone).append(", invAddress=").append(invAddress).
		append(", invTitle=").append(invTitle).append(", invTaxId=").append(invTaxId).
		append(", token=").append(token).append("]"); 
		
		LOG.info(sf.toString());
		try {
			if (StringUtils.isBlank(payFrom) || StringUtils.isBlank(payOrderId) ||
				StringUtils.isBlank(name) || StringUtils.isBlank(mobile) ||
				StringUtils.isBlank(address) || money<=0 ||
				StringUtils.isBlank(currency) || StringUtils.isBlank(orderTime)) {
				code = Config.RETURN_INPUT_EMPTY;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			Customer customer = customerManager.findByPayFrom(payFrom);
			if (!token.equalsIgnoreCase(MD5Util.crypt(payFrom+ payOrderId +mobile+money+ orderTime + invType +customer.getKey()))) {
				code = Config.RETURN_TOKEN_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			
			if(invType == 2 && StringUtils.isBlank(email))
			{
				code = Config.RETURN_INPUT_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			
			if(invType > 0 &&
					(
							StringUtils.isBlank(invName) ||
							StringUtils.isBlank(invPhone) ||
							StringUtils.isBlank(invAddress)
					)
			) {
				code = Config.RETURN_INPUT_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			
			if(invType == 3 &&
					(
							StringUtils.isBlank(invTitle) ||
							StringUtils.isBlank(invTaxId)
					)
			) {
				code = Config.RETURN_INPUT_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			Member member = saveMember(uid, payFrom, name, mobile, address, new Timestamp(sdf.parse(orderTime).getTime()), false);
			
			if(StringUtils.isBlank(uid)) {
				uid = member.getUid();
			}
			
			Payment pay = savePayment(money, address, payFrom, new Timestamp(sdf.parse(orderTime).getTime()), 0, name, mobile, uid, 1, payOrderId, currency);
			if(StringUtils.isBlank(uid)) {
				member.setUid(member.getId()+"");
				memberManager.update(member);
			}
			saveInvoice(pay.getOrderId(), uid, payFrom, invType, email, invName, invPhone, invAddress, invTitle, invTaxId, new Timestamp(sdf.parse(orderTime).getTime()));
			
			result.put("orderId", pay.getOrderId());
			
			result.put("token", MD5Util.crypt(pay.getOrderId()+token+customer.getKey()));
		} catch(Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		result.put("code", code);
		result.put("message", Config.RETURN_RESULT.get(code).toString());
		LOG.info("result="+result);
		return result;
	}

	private Invoice saveInvoice(String orderId, String uid, String payFrom, int invoiceType, String email, String invoiceName, String invoicePhone, String invoiceAddress, String invoiceTitle, String taxId, Timestamp createTime) {
		Invoice invoice = new Invoice();
		invoice.setOrderId(orderId);
		invoice.setUid(uid);
		invoice.setPayFrom(payFrom);
		invoice.setInvoiceType(invoiceType);
		invoice.setEmail(email);
		invoice.setInvoiceName(invoiceName);
		invoice.setInvoicePhone(invoicePhone);
		invoice.setInvoiceAddress(invoiceAddress);
		invoice.setInvoiceTitle(invoiceTitle);
		invoice.setTaxId(taxId);
		invoice.setCreateTime(createTime);
//		invoice.setLastPayTime(lastPayTime);
//		invoice.setStatus(status);
		invoiceManager.add(invoice);
		return invoice;
	}

	private Member saveMember(String uid, String payFrom, String name, String mobile, String address, Timestamp lastPayTime, boolean newMemberAuth) {
		Member member = null;
		if(StringUtils.isNotBlank(uid)) {
			member = memberManager.findByUidAndPayFrom(uid, payFrom);
		} else {
			member = memberManager.findByNameMobilePayFrom(name, mobile, payFrom);
		}
		
		if (member == null) {
			member = new Member();
			member.setUid(uid);
			member.setPayFrom(payFrom);
			member.setName(name);
			member.setMobile(mobile);
			member.setAddress(address);
//			member.setCreateTime(createTime);
			member.setLastPayTime(lastPayTime);
			
			if(newMemberAuth) {
				member.setStatus(1);
			}
			
			member = memberManager.add(member);
		} else {
//			member.setUid(uid);
//			member.setPayFrom(payFrom);
			member.setName(name);
			member.setMobile(mobile);
			member.setAddress(address);
			member.setLastPayTime(lastPayTime);
			memberManager.update(member);
		}
		
		if(StringUtils.isBlank(uid)) {
			member.setUid(member.getId()+"");
			memberManager.updateUid(member.getUid(), name, mobile, payFrom);
		}
		
		return member;
	}

	private Payment savePayment(int money, String userAddress, String payFrom, Timestamp createTime, int restoreStatus, String name, String userMobile, String uid, int payStatus, String payOrderId, String currency) {
		Payment payment = new Payment();
		String sorderId = "TWG"+sdf0.format(new Date());
		payment.setMoney(money);
		payment.setCurrency(currency);
		payment.setUserAddress(userAddress);
//		payment.setCarId(carId);
		payment.setPayFrom(payFrom);
		payment.setCreateTime(createTime);
		payment.setRestoreStatus(restoreStatus);
		payment.setName(name);
		payment.setUserMobile(userMobile);
//		payment.setRemark(remark);
//		payment.setGameCode(gameCode);
		payment.setUid(uid);
//		payment.setAccountStatus(accountStatus);
		payment.setPayStatus(payStatus);
		payment.setOrderId(sorderId);
//		payment.setCancelTime(cancelTime);
		payment.setPayOrderId(payOrderId);
//		payment.setDriverId(driverId);
		
		
		int cnt = payManager.countPay(uid);
		payment.setIsFirst((cnt>0)?0:1);
		
//		payment.setPayTime(payTime);
		payment = payManager.add(payment);
		return payment;
	}
	
	private Payment savePaymentHK(String gameCode, int money, String userAddress, String payFrom, 
			Timestamp createTime, int restoreStatus, String name, String userMobile, 
			String uid, int payStatus, int accountStatus, String payOrderId, String currency) {
		Payment payment = new Payment();
		String sorderId = "TWG" + sdf0.format(new Date());
		payment.setMoney(money);
		payment.setCurrency(currency);
		payment.setUserAddress(userAddress);

		payment.setPayFrom(payFrom);
		payment.setCreateTime(createTime);
		payment.setRestoreStatus(restoreStatus);
		payment.setName(name);
		payment.setUserMobile(userMobile);

		payment.setGameCode(gameCode);
		payment.setUid(uid);
		payment.setAccountStatus(accountStatus);
		payment.setPayStatus(payStatus);
		payment.setOrderId(sorderId);

		payment.setPayOrderId(payOrderId);		
		
		int cnt = payManager.countPay(uid);
		payment.setIsFirst((cnt>0)?0:1);
		
		payment = payManager.addPaid(payment, "4");
		return payment;
	}
	
	@RequestMapping(value = "/queryOrder")
	public @ResponseBody Map<String, Object> queryOrder(String payFrom, String orderId, String token) {
		Map<String, Object> result = new HashMap<String, Object>();
		String code = Config.RETURN_SUCCESS;
		
		StringBuffer sf = new StringBuffer();
		sf.append("============== OrderApi queryOrder ====================\n");
		sf.append("input param [payFrom=").append(payFrom).append(",orderId=").append(orderId).append(",token=").append(token).append("]"); 
		
		LOG.info(sf.toString());
		try {
			Customer customer = customerManager.findByPayFrom(payFrom);
			if (!MD5Util.crypt(payFrom+ orderId +customer.getKey()).equalsIgnoreCase(token)) {
				code = Config.RETURN_TOKEN_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			
			Payment pay = payManager.findByPayFromPayOrderId(payFrom, orderId);
			
			if (pay == null) {
				code = Config.RETURN_DATA_NOT_FOUND;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			} else if((System.currentTimeMillis()-pay.getCreateTime().getTime()) > 7200000) {
				code = Config.RETURN_TIME_OUT;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			
			result.put("orderId", pay.getOrderId());
			result.put("status", pay.getPayStatus());
			
			result.put("token", MD5Util.crypt(pay.getOrderId()+pay.getPayStatus()+token+customer.getKey()));
		} catch(Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		result.put("code", code);
		result.put("message", Config.RETURN_RESULT.get(code).toString());
		LOG.info("result="+result);
		return result;
	}
	
	@RequestMapping(value = "/rebate")
	public @ResponseBody Map<String, Object> rebate(
			String payFrom, String orderId, 
			String status, String statusTime,
			String token) {
		Map<String, Object> result = new HashMap<String, Object>();
		String code = Config.RETURN_SUCCESS;
		
		StringBuffer sf = new StringBuffer();
		sf.append("============== OrderApi rebate ====================\n");
		sf.append("input param [payFrom=").append(payFrom).append(",orderId=").append(orderId).append(",status=").append(status); 
		sf.append(",statusTime=").append(statusTime).append("]"); 
		
		LOG.info(sf.toString());
		try {
			Customer customer = customerManager.findByPayFrom(payFrom);
			if (!MD5Util.crypt(payFrom+orderId+status+statusTime+customer.getKey()).equalsIgnoreCase(token)) {
				code = Config.RETURN_TOKEN_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			
			PaymentForView pay = payManager.findByOrderId(orderId);
			
			if (pay == null) {
				code = Config.RETURN_DATA_NOT_FOUND;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			} 
			LOG.info("pay status="+pay.getPayStatus());
			if (pay.getPayStatus()==4 || pay.getPayStatus()==6) {
				if ("1".equals(status)) { //已發點
					payManager.updatePayStatus(pay.getId(), 5);
				} else { //發點失敗
					payManager.updatePayStatus(pay.getId(), 6);
				}
			} else {
				code = Config.RETURN_FAIL;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
			}
		} catch(Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		result.put("code", code);
		result.put("message", Config.RETURN_RESULT.get(code).toString());
		LOG.info("result="+result);
		return result;
	}
		
	/**
	 * 香港到府建單發放
	 * 
	 * @param payFrom
	 * @param payOrderId
	 * @param uid
	 * @param name
	 * @param mobile
	 * @param address
	 * @param money
	 * @param currency
	 * @param orderTime
	 * @param invType
	 * @param email
	 * @param invName
	 * @param invPhone
	 * @param invAddress
	 * @param invTitle
	 * @param invTaxId
	 * @param token
	 * @return
	 */
	@RequestMapping(value = "/hkorder")
	public @ResponseBody Map<String, String> order(
			@RequestParam("gameCode") String gameCode,
			@RequestParam("payFrom") String payFrom,
			@RequestParam("orderId") String payOrderId, //orderId金流方 
			@RequestParam(value = "uid", required = false, defaultValue = "") String uid,
			@RequestParam("name") String name,
			@RequestParam("mobile") String mobile,
			@RequestParam("address") String address,
			@RequestParam("money") int money,
			@RequestParam("currency") String currency,
			@RequestParam("orderTime") String orderTime,			
			@RequestParam("token") String token ) {
		Map<String, String> result = new HashMap<String, String>();
		String code = Config.RETURN_SUCCESS;
		// dump input params
		StringBuffer sf = new StringBuffer();
		sf.append("============== HK Home OrderApi order ====================\n");
		sf.append("input param [payFrom=").append(payFrom).append(", orderId=").append(payOrderId);
		sf.append(", uid=").append(uid).append(", name=").append(name).append(",gameCode=").append(gameCode).
		append(", mobile=").append(mobile).append(", address=").append(address).
		append(", money=").append(money).append(", currency=").append(currency).append(", orderTime=").append(orderTime).
		append(", token=").append(token).append("]"); 
		
		LOG.info(sf.toString());
		try {
			// validate input params
			if(StringUtils.isBlank(payFrom) || StringUtils.isBlank(payOrderId) ||
				StringUtils.isBlank(name) || money<=0 ||
				StringUtils.isBlank(currency) || StringUtils.isBlank(orderTime)) {
				code = Config.RETURN_INPUT_EMPTY;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			// validate token
			LOG.info("token source1=" + payFrom + payOrderId + mobile + money + orderTime + appKey);
			LOG.info("token1 =" + token);
			
			if(!token.equalsIgnoreCase(MD5Util.crypt(payFrom + payOrderId + mobile + money + orderTime + appKey))) {
				code = Config.RETURN_TOKEN_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}			
			// save member(新會員直接已認證)
			Member member = saveMember(uid, payFrom, name, mobile, address, new Timestamp(sdf.parse(orderTime).getTime()), true);
			
			if(StringUtils.isBlank(uid)) {
				uid = member.getUid();
			}
			// save payment(已發點)
			Payment pay = savePaymentHK(gameCode, money, address, payFrom, 
					new Timestamp(sdf.parse(orderTime).getTime()), 0, name, mobile, uid, 5, 0, 
					payOrderId, currency);
			
			if(StringUtils.isBlank(uid)) {
				member.setUid(member.getId() + "");
				memberManager.update(member);
			}
			
			// return
			result.put("orderId", pay.getOrderId());			
			result.put("token", MD5Util.crypt(pay.getOrderId() + token + appKey));
		} 
		catch(Exception e) {
			LOG.error("err:", e);
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		result.put("code", code);
		result.put("message", Config.RETURN_RESULT.get(code).toString());
		LOG.info("result="+result);
		return result;
	}
	
	public static void main(String[] args) {
		System.out.println(MD5Util.crypt("gash"+ "GP150708A0000003" +"0955786368"+5000+ "2015-07-09 14:24:55" + 0 +"676c5a5b0b3f73784cd00c1393c90a38"));
	}
}