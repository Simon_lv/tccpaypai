package tw.com.twgame.controller.api;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.com.twgame.common.encrypt.MD5Util;
import tw.com.twgame.constants.Config;
import tw.com.twgame.controller.BaseController;
import tw.com.twgame.model.Invoice;
import tw.com.twgame.model.Member;
import tw.com.twgame.model.Payment;
import tw.com.twgame.service.InvoiceManager;
import tw.com.twgame.service.MemberManager;
import tw.com.twgame.service.PaymentManager;









@Controller
@Scope("request")
public class PaymentApi extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(PaymentApi.class);
	@Autowired
	@Qualifier("appKey")
	private String appKey;
	
	@Autowired
	protected PaymentManager payManager;
	
	@Autowired
	protected MemberManager memberManager;
	
	@Autowired
	protected InvoiceManager invoiceManager;
	
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final SimpleDateFormat sdf0 = new SimpleDateFormat("yyyyMMddHHmmssS");
	
	
	@RequestMapping(value = "/pay")
	public @ResponseBody Map<String, String> pay(
			@RequestParam("payFrom") String payFrom,  //標識碼
			@RequestParam("orderId") String orderId,  //訂單編號
			@RequestParam("uid") String uid,
			@RequestParam("gameCode") String gameCode,
			@RequestParam("money") int money,
			@RequestParam("currency") String currency,
			@RequestParam("name") String name,
			@RequestParam("mobile") String mobile,
			@RequestParam("address") String address,
			@RequestParam("date") String date,
			@RequestParam("token") String token,
			@RequestParam("invoiceType") int invoiceType,
			@RequestParam("invoiceName") String invoiceName,
			@RequestParam("invoiceMobile") String invoiceMobile,
			@RequestParam("invoiceAddress") String invoiceAddress,
			@RequestParam("invoiceTitle") String invoiceTitle,
			@RequestParam("invoiceTaxId") String invoiceTaxId
		) {
		Map<String, String> result = new HashMap<String, String>();
		String code = Config.RETURN_SUCCESS;
		
		StringBuffer sf = new StringBuffer();
		sf.append("============== PaymentApi pay ====================\n");
		sf.append("input param [payFrom=").append(payFrom).append(", orderId=").append(orderId);
		sf.append(", uid=").append(uid).append(", gameCode=").append(gameCode).append(", money=").append(money).append(", currency=").append(currency);
		sf.append(", name=").append(name).append(", mobile=").append(mobile).append(", address=").append(address);
		sf.append(", date=").append(date).append(", token=").append(token).
		append(", invoiceType=").append(invoiceType).
		append(", invoiceName=").append(invoiceName).
		append(", invoiceMobile=").append(invoiceMobile).
		append(", invoiceAddress=").append(invoiceAddress).
		append(", invoiceTitle=").append(invoiceTitle).
		append(", invoiceTaxId=").append(invoiceTaxId).append("]");
		
		LOG.info(sf.toString());
		
		
		try {
			if (!token.equalsIgnoreCase(MD5Util.crypt(payFrom+orderId+uid+gameCode+money+appKey))) {
				code = Config.RETURN_TOKEN_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			
			Timestamp payTime = new Timestamp(sdf.parse(date).getTime());
			Member member = memberManager.findByUidAndPayFrom(uid, payFrom);
			if (member == null) {
				member = new Member();
				member.setUid(uid);
				member.setPayFrom(payFrom);
				member.setName(name);
				member.setMobile(mobile);
				member.setAddress(address);
				member.setLastPayTime(payTime);
				
				memberManager.add(member);
			} else {
//				member.setUid(uid);
//				member.setPayFrom(payFrom);
				member.setName(name);
				member.setMobile(mobile);
				member.setAddress(address);
				member.setLastPayTime(payTime);
				memberManager.update(member);
			}
			
			String sorderId = "TWG"+sdf0.format(new Date());
			Payment pay = new Payment();
			pay.setOrderId(sorderId);
			pay.setPayFrom(payFrom);
			pay.setPayOrderId(orderId);
			pay.setUid(uid);
			pay.setGameCode(gameCode);
			pay.setName(name);
			pay.setMoney(money);
			pay.setCurrency(currency);
			pay.setUserMobile(mobile);
			pay.setUserAddress(address);
			pay.setCreateTime(payTime);
			int cnt = payManager.countPay(uid);
			pay.setIsFirst((cnt>0)?0:1);
			pay = payManager.add(pay);
			if ("TWD".equalsIgnoreCase(currency)) {
				saveInvoice(sorderId, uid, payFrom, invoiceType, invoiceName, 
						invoiceMobile, invoiceAddress, invoiceTitle, invoiceTaxId, new Timestamp(System.currentTimeMillis()));
			}
			result.put("orderId", pay.getOrderId());
			
		} catch(Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		result.put("code", code);
		result.put("message", Config.RETURN_RESULT.get(code).toString());
		LOG.info("result="+result);
		return result;
	}
//	test data
//	public static void main(String[] args) {
//		String carId = "99721";
//		System.out.println("carId = 99721 and type=1 token=  "+MD5Util.crypt(carId+1+KEY));
//		
//		System.out.println("carId = 99721 and type=2 token=  "+MD5Util.crypt(carId+2+KEY));
//
//		System.out.println("carId = 99721 and type=3 token=  "+MD5Util.crypt(carId+3+KEY));
//	}
	
	private Invoice saveInvoice(String orderId, String uid, String payFrom, int invoiceType, String invoiceName, String invoicePhone, String invoiceAddress, String invoiceTitle, String taxId, Timestamp createTime) {
		Invoice invoice = new Invoice();
		invoice.setOrderId(orderId);
		invoice.setUid(uid);
		invoice.setPayFrom(payFrom);
		invoice.setInvoiceType(invoiceType);
		invoice.setInvoiceName(invoiceName);
		invoice.setInvoicePhone(invoicePhone);
		invoice.setInvoiceAddress(invoiceAddress);
		invoice.setInvoiceTitle(invoiceTitle);
		invoice.setTaxId(taxId);
		invoice.setCreateTime(createTime);
//		invoice.setLastPayTime(lastPayTime);
//		invoice.setStatus(status);
		invoiceManager.add(invoice);
		return invoice;
	}
	
	public static void main(String[] args) {
		SimpleDateFormat sdf0 = new SimpleDateFormat("yyyyMMddHHmmssS");
		System.out.println(sdf0.format(new Date()));
		
	}
}
