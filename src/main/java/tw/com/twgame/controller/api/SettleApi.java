package tw.com.twgame.controller.api;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tw.com.twgame.common.encrypt.MD5Util;
import tw.com.twgame.constants.Config;
import tw.com.twgame.controller.BaseController;
import tw.com.twgame.model.Driver;
import tw.com.twgame.service.DriverManager;
import tw.com.twgame.service.PaymentManager;









@Controller
@Scope("request")
public class SettleApi extends BaseController {
	private static final Logger LOG = LoggerFactory.getLogger(SettleApi.class);
	@Autowired
	@Qualifier("appKey")
	private String appKey;
	
	@Autowired
	protected DriverManager drManager;
	
	@Autowired
	protected PaymentManager payManager;
	
	
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@RequestMapping(value = "/settle")
	public @ResponseBody Map<String, String> settle(
			@RequestParam("paymentId") String orderId,  //訂單編號
			@RequestParam("carId") String carId,
			@RequestParam("money") int money,
			@RequestParam("date") String date,
			@RequestParam("token") String token
		) {
		Map<String, String> result = new HashMap<String, String>();
		String code = Config.RETURN_SUCCESS;
		
		StringBuffer sf = new StringBuffer();
		sf.append("============== SettleApi settle ====================\n");
		sf.append("input param [paymentId:").append(orderId).append(", carId=").append(carId);
		sf.append(", money=").append(money).append(", date=").
		   append(date).append(", token=").append(token).append("]");
		
		LOG.info(sf.toString());
		
		
		try {
			if (!token.equalsIgnoreCase(MD5Util.crypt(orderId+carId+money+appKey))) {
				code = Config.RETURN_TOKEN_ERROR;
				result.put("code", code);
				result.put("message", Config.RETURN_RESULT.get(code).toString());
				return result;
			}
			Driver driver = drManager.findBycarId(carId);
			if (driver == null) {
				code = Config.RETURN_DATA_NOT_FOUND;
				result.put("code", code);
				result.put("message", "driver not found");
				return result;
			}

			driver.setNonRestore(driver.getNonRestore()-money > 0? (driver.getNonRestore()-money) : 0);
			driver.setCanSell(driver.getCredit() > driver.getNonRestore()? 1 : 2);
			drManager.update(driver);
			
			String orderIds[] = orderId.split(",");
			Date restoreTime = sdf.parse(date);
			payManager.updateRestore(orderIds, restoreTime, 2);
			
		} catch(Exception e) {
			e.printStackTrace();
			code = Config.RETURN_SYSTEM_ERROR;
			result.put("code", code);
			result.put("message", Config.RETURN_RESULT.get(code).toString());
		}
		result.put("code", code);
		result.put("message", Config.RETURN_RESULT.get(code).toString());
		LOG.info("result="+result);
		return result;
	}
//	test data
//	public static void main(String[] args) {
//		String carId = "99721";
//		System.out.println("carId = 99721 and type=1 token=  "+MD5Util.crypt(carId+1+KEY));
//		
//		System.out.println("carId = 99721 and type=2 token=  "+MD5Util.crypt(carId+2+KEY));
//
//		System.out.println("carId = 99721 and type=3 token=  "+MD5Util.crypt(carId+3+KEY));
//	}
	
	public static void main(String[] args) {
		String paymentId = "20150216122219101,20150216122219102";
		String carId = "99770";
		int money = 200;
		String KEY = "";
		System.out.println(MD5Util.crypt(paymentId+carId+money+KEY));
		
	}
}
